package org.example.myfirstspringapp.restcontroller;

import lombok.RequiredArgsConstructor;
import org.example.myfirstspringapp.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/teacher")
//@RequiredArgsConstructor
public class TeacherRestController {
    // add teacherService
    private final TeacherService teacherService;
    @Autowired
    public TeacherRestController(@Qualifier("teacherServiceImpl22") TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping("/create-subject")
    public String createSubject() {
        teacherService.createSubject();
        return "created subject successfully!";
    }
}
