package org.example.myfirstspringapp.service.serviceImpl;

import org.example.myfirstspringapp.service.StudentService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
//@Primary
public class StudentServiceImpl2 implements StudentService {
    @Override
    public void createStudent() {
        System.out.println("Create Student Service V2");
    }

    @Override
    public void deleteStudent() {
        System.out.println("Delete Student Service V2");
    }
}
