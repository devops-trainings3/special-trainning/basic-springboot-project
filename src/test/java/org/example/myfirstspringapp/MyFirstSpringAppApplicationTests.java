package org.example.myfirstspringapp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

// we are most likely to use the Field Injection here!
@SpringBootTest
class MyFirstSpringAppApplicationTests {

    @Test
    void contextLoads() {
    }

}
